from django.test import TestCase, Client
from django.urls import resolve
from .views import story
# from .models import Status
# from .forms import StatusForm

# from selenium import webdriver
# import unittest
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UnitTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, story)

    def test_home_using_template_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story.html')

# class FuncionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')

#         self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
#         super(FuncionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(FuncionalTest, self).tearDown()
    
#     def test_functional(self):
#         self.browser.get('http://localhost:8000/')

#         #Test Judul
#         self.assertIn('pepewtest', self.browser.title)

#         #Test Tulisan 'Halo'
#         header_text = self.browser.find_element_by_tag_name('h1').text
#         self.assertIn('Halo', header_text)

# if __name__ == '__main__':
#     unittest.main(warnings='ignore')