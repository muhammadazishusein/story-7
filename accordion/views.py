from django.shortcuts import render, redirect


# Create your views here.
def story(request):
    return render(request, 'story.html')